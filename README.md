## Running

```bash
$ npm install
$ npm start
```

The server should be live at <http://localhost:3000>.

## How to get from react-redux todos example to this project

### Install `semantic-ui` and `semantic-ui-react`

```bash
$ npm install --save semantic-ui-react
$ npm install --save-dev semantic-ui
```