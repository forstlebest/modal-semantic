import React, { Component } from 'react';
import { Button, Input, Grid, Form, Modal, Dropdown } from 'semantic-ui-react'

import './Modal.css';

const taxList = [
  {
    type: 'Income Tax',
    to: 'IRPEF',
    account: '1210010 - Lorem ipsum',
    rate: 20,
    value: 100,
    social: '',
  },{
    type: 'Income Tax Regional',
    to: 'IRPEF',
    account: '1210010 - Lorem ipsum',
    rate: 4,
    value: 20,
    social: '',
  },{
    type: 'Income Tax Communal',
    to: 'IRPEF',
    account: '1210010 - Lorem ipsum',
    rate: 4,
    value: 30,
    social: '',
  },{
    type: 'Social Security - Paid by Percipienti',
    to: 'INPS',
    account: '1210010 - Lorem ipsum',
    rate: 33.33,
    value: 10,
    social: 8.33,
  },{
    type: 'Social Security - Paid by Percipienti',
    to: 'INPS',
    account: '1210010 - Lorem ipsum',
    rate: 66.67,
    value: 10,
    social: 16.66,
  }
];

const invoiceIds = [
  {key: '1', text: 'invoice id1', value: 'invoice id1'},
  {key: '2', text: 'invoice id2', value: 'invoice id2'},
]
const taxTypes = [
  {key: '1', text: 'Unpaid', value: 1},
  {key: '2', text: 'Paid', value: 2},
  {key: '3', text: 'Archived', value: 3},
];
const numberValues = ['amount', 'vat', 'otherExpense', 'security', 'miscel'];

class ModalComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      supplier: '',
      invoiceId: '',
      taxCategoy: '',
      amount: 0,
      totalAmount: 0,
      vat: 0,
      otherExpense: 0,
      security: 0,
      miscel: 0,
    };
  }

  componentDidMount() {
    // We can call API to get tax data
  }

  submit = () => {
    // Submit Form
  }

  toggleModal = () => {
    this.setState(({ showModal }) => ({ showModal: !showModal }));
  }

  changeProperty = (type, value) => {
    let updatedValue = value;
    if (numberValues.includes(type)) {
      updatedValue = parseInt(updatedValue.replace(/,/g, ''), 10).toLocaleString();
    }
    this.setState({ [type]: updatedValue });
  }

  changeInvoiceId = (event, { value }) => {
    this.setState({ invoiceId: value });
  }

  selectTaxTypes = (event, data) => {
    this.setState({ taxTypes: data.value });
  }

  numberWithCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
  }

  renderMiddleTable = () => {
    return (
      <table className="middle-table">
        <tbody>
          <tr>
            <th>Due Date</th>
            <th>Status</th>
            <th>Amount</th>
            <th>VAT</th>
            <th>Other expenses</th>
            <th>Social Security</th>
            <th>Miscelaneous</th>
            <th>Taxable Amount</th>
          </tr>
          <tr style={{ height: 40 }}>
            <td className="middle-table-col">05.12.2018</td>
            <td className="middle-table-col right-align">Paid</td>
            <td className="middle-table-col right-align">
              <input className="tax-value" value={this.state.totalAmount} onChange={(event) => this.changeProperty('totalAmount', event.target.value)}/>
            </td>
            <td className="middle-table-col right-align">{this.state.vat}</td>
            <td className="middle-table-col right-align">{this.state.otherExpense}</td>
            <td className="middle-table-col right-align">{this.state.security}</td>
            <td className="middle-table-col right-align">{this.state.miscel}</td>
            <td className="middle-table-col right-align" style={{ backgroundColor: '#F4F6F8' }}>1000</td>
          </tr>
        </tbody>
      </table>
    )
  }

  renderTaxTable = () => {
    return (
      <table className="tax-table">
        <tbody>
          <tr className="tax-header">
            <th className="tax-first-col">Tax Type</th>
            <th>Pay To</th>
            <th>Account</th>
            <th>Rate%</th>
            <th>Income tax</th>
            <th>Social Security</th>
          </tr>
          {taxList.map((tax, i) => (
            <tr key={i} className="tax-row">
              <td className="tax-first-col">{tax.type}</td>
              <td>{tax.to}</td>
              <td>{tax.account}</td>
              <td>{tax.rate}</td>
              <td>{tax.value}</td>
              <td>{tax.social}</td>
            </tr>
          ))}
          <tr className="tax-last-row">
            <td className="tax-first-col-total">TOTAL:</td>
            <td />
            <td />
            <td />
            <td>15000</td>
            <td>2500</td>
          </tr>
        </tbody>
      </table>
    )
  }

  renderItemTable = () => {
    return (
      <div>
        {this.renderMiddleTable()}
        {this.renderTaxTable()}
      </div>
    )
  }

  render() {
    return (
      <Modal dimmer="inverted" open={this.state.showModal} trigger={<Button onClick={this.toggleModal}>Show Modal</Button>}>
        <Modal.Header>Edit invoice details<div className="close" onClick={this.toggleModal} /></Modal.Header>
        <Modal.Content>
          <Form>
            <Grid columns='equal'>
              <Grid.Column width={6}>
                <Form.Field>
                  <label>Percipenti (Supplier)</label>
                  <Input onChange={(event) => this.changeProperty('supplier', event.target.value)}/>
                </Form.Field>
              </Grid.Column>
              <Grid.Column>
                <Form.Field>
                  <label>Invoice ID</label>
                  <Dropdown options={invoiceIds} selection onChange={this.changeInvoiceId} />
                </Form.Field>
              </Grid.Column>
              <Grid.Column width={6}>
                <Form.Field>
                  <label>Tax Category</label>
                  <Dropdown options={taxTypes} selection multiple onChange={this.selectTaxTypes} />
                </Form.Field>
              </Grid.Column>
            </Grid>

            <Grid columns='equal'>
              <Grid.Column width={3}>
                <Form.Field>
                  <label className="tax-label">Total Amount</label>
                  <input className="tax-value" value={this.state.amount} onChange={(event) => this.changeProperty('amount', event.target.value)}/>
                </Form.Field>
              </Grid.Column>
              <Grid.Column width={3}>
                <Form.Field>
                  <label className="tax-label">Total VAT</label>
                  <input className="tax-value" value={this.state.vat} onChange={(event) => this.changeProperty('vat', event.target.value)}/>
                </Form.Field>
              </Grid.Column>
              <Grid.Column width={4}>
                <Form.Field>
                  <label className="tax-label">Total Other expenses</label>
                  <input className="tax-value" value={this.state.otherExpense} onChange={(event) => this.changeProperty('otherExpense', event.target.value)}/>
                </Form.Field>
              </Grid.Column>
              <Grid.Column width={3}>
                <Form.Field>
                  <label className="tax-label">Rivalsa</label>
                  <input className="tax-value" value={this.state.security} onChange={(event) => this.changeProperty('security', event.target.value)}/>
                </Form.Field>
              </Grid.Column>
              <Grid.Column width={3}>
                <Form.Field>
                  <label className="tax-label">Miscelaneous</label>
                  <input className="tax-value" value={this.state.miscel} onChange={(event) => this.changeProperty('miscel', event.target.value)}/>
                </Form.Field>
              </Grid.Column>
            </Grid>
            {this.renderItemTable()}
          </Form>
          <h5 className="total-label">Total Wittholding tax to pay (ritenute): 330,00</h5>
        </Modal.Content>
        <Modal.Actions>
          <Button>Cancel</Button>
          <Button onClick={this.submit} primary>Save</Button>
        </Modal.Actions>
      </Modal>
    )
  }
}

export default ModalComponent;