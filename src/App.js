import React from 'react'
import ModalComponent from './Modal';

import './App.css'

const App = () => (
  <div className="AppContainer">
    <h1>My Todo List</h1>
    <div className="App">
      <ModalComponent />
    </div>
  </div>
)

export default App
